#include <gst/gst.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "controller.h"

int main(int argc, char* argv[])
{
    gst_init(nullptr, nullptr);

    auto _sink = gst_element_factory_make("qmlglsink", nullptr);
    Q_ASSERT(_sink);
    QGuiApplication application(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterSingletonInstance("org.kde.test", 1, 0, "Controller", new Controller);
    engine.load("../main.qml");
    return application.exec();
}
