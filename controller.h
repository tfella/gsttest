#pragma once

#include <QObject>
#include <QQuickItem>

#include <gst/gst.h>

class Controller : public QObject
{
    Q_OBJECT
public:
    Controller(QObject *parent = nullptr) : QObject(parent) {}
    Q_INVOKABLE void setVideoItem(QQuickItem *item) {
        auto pipeline = gst_pipeline_new(nullptr);
        auto videotestsrc = gst_element_factory_make("videotestsrc", nullptr);
        auto glupload = gst_element_factory_make("glupload", nullptr);
        auto glcolorconvert = gst_element_factory_make("glcolorconvert", nullptr);
        auto qmlglsink = gst_element_factory_make("qmlglsink", nullptr);
        auto glsinkbin = gst_element_factory_make("glsinkbin", nullptr);
        g_object_set(qmlglsink, "widget", item, nullptr);
        g_object_set(glsinkbin, "sink", qmlglsink, nullptr);

        gst_bin_add_many(GST_BIN(pipeline), videotestsrc, glupload, glcolorconvert, glsinkbin, nullptr);
        qWarning() << gst_element_link_many(videotestsrc, glupload, glcolorconvert, glsinkbin, nullptr);
        gst_element_set_state(pipeline, GST_STATE_PLAYING);
    }
};
