import QtQuick 2.15
import QtQuick.Controls 2.15

import org.freedesktop.gstreamer.GLVideoItem 1.0
import org.kde.test 1.0

ApplicationWindow {
    width: 400
    height: 400
    visible: true
    GstGLVideoItem {
        id: item
        anchors.fill: parent
        Component.onCompleted: Controller.setVideoItem(item)
    }
}
